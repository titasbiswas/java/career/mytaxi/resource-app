package com.mytaxi.dataaccessobject;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.fasterxml.jackson.databind.deser.DataFormatReaders.Match;
import com.mytaxi.datatransferobject.DriverSearchDTO;
import com.mytaxi.domainobject.DriverDO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DriverRepositoryImpl implements DriverRepositoryCustom
{

    @PersistenceContext
    private EntityManager entityManager;

    private Map<String, Object> paramMap = new LinkedHashMap<>();


    @Override
    public List<DriverDO> searchDriver(DriverSearchDTO searchDTO)
    {
        log.info("Start - filer matches;  filer criteria: {}", searchDTO);

        Query query = entityManager.createQuery(buildQueryString(searchDTO));

        for (Map.Entry<String, Object> paramEntry : paramMap.entrySet())
        {
            query.setParameter(paramEntry.getKey(), paramEntry.getValue());
            log.info("Search Parameters - Name: " + paramEntry.getKey() + " Value: " + paramEntry.getValue());
        }
        paramMap.clear();
        List<DriverDO> matches = query.getResultList();
        log.info("End - filer matches; result size: {}", matches.size());
        return matches;
    }


    private String buildQueryString(DriverSearchDTO searchDTO)
    {

        StringBuilder queryStr = new StringBuilder("select d from DriverDO d");

        int noOfClause = 0;

        if (null != searchDTO.getOnlineStatus())
        {
            noOfClause++;
            if (noOfClause == 1)
            {
                queryStr.append(" where ");
            }
            else if (noOfClause > 1)
            {
                queryStr.append(" and ");
            }

            queryStr.append("d.onlineStatus = :onlineStatus");
            paramMap.put("onlineStatus", searchDTO.getOnlineStatus());
        }

        if (null != searchDTO.getUserName())
        {
            noOfClause++;
            if (noOfClause == 1)
            {
                queryStr.append(" where ");
            }
            else if (noOfClause > 1)
            {
                queryStr.append(" and ");
            }

            queryStr.append("d.username = :username");
            paramMap.put("username", searchDTO.getUserName());
        }

        if (null != searchDTO.getCar().getConvertible())
        {
            noOfClause++;
            if (noOfClause == 1)
            {
                queryStr.append(" where ");
            }
            else if (noOfClause > 1)
            {
                queryStr.append(" and ");
            }

            queryStr.append("d.car.convertible = :convertible");
            paramMap.put("convertible", searchDTO.getCar().getConvertible());
        }

        if (null != searchDTO.getCar().getEngineType())
        {
            noOfClause++;
            if (noOfClause == 1)
            {
                queryStr.append(" where ");
            }
            else if (noOfClause > 1)
            {
                queryStr.append(" and ");
            }

            queryStr.append("d.car.engineType = :engineType");
            paramMap.put("engineType", searchDTO.getCar().getEngineType());
        }
        
        if (null != searchDTO.getCar().getLicencePlate())
        {
            noOfClause++;
            if (noOfClause == 1)
            {
                queryStr.append(" where ");
            }
            else if (noOfClause > 1)
            {
                queryStr.append(" and ");
            }

            queryStr.append("d.car.licencePlate = :licencePlate");
            paramMap.put("licencePlate", searchDTO.getCar().getLicencePlate());
        }
        
        if (null != searchDTO.getCar().getManufacturer().getManufacturedBy())
        {
            noOfClause++;
            if (noOfClause == 1)
            {
                queryStr.append(" where ");
            }
            else if (noOfClause > 1)
            {
                queryStr.append(" and ");
            }

            queryStr.append("d.car.manufacturer.manufacturedBy = :manufacturedBy");
            paramMap.put("manufacturedBy", searchDTO.getCar().getManufacturer().getManufacturedBy());
        }
        
        if (null != searchDTO.getCar().getRating())
        {
            noOfClause++;
            if (noOfClause == 1)
            {
                queryStr.append(" where ");
            }
            else if (noOfClause > 1)
            {
                queryStr.append(" and ");
            }

            queryStr.append("d.car.rating = :rating");
            paramMap.put("rating", searchDTO.getCar().getRating());
        }
        
        if (null != searchDTO.getCar().getSeatCount())
        {
            noOfClause++;
            if (noOfClause == 1)
            {
                queryStr.append(" where ");
            }
            else if (noOfClause > 1)
            {
                queryStr.append(" and ");
            }

            queryStr.append("d.car.seatCount = :seatCount");
            paramMap.put("seatCount", searchDTO.getCar().getSeatCount());
        }

        return queryStr.toString();
    }

}
