package com.mytaxi.dataaccessobject;

import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.OnlineStatus;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Database Access Object for driver table.
 * <p/>
 */
public interface DriverRepository extends CrudRepository<DriverDO, Long>, DriverRepositoryCustom
{

    List<DriverDO> findByOnlineStatus(OnlineStatus onlineStatus);
    
    @Query(nativeQuery=true, value="update driver set car_id= ?2 where id= ?1 and online_status='ONLINE'")
    @Modifying
    @Transactional
    void allocateCar(Long driverId, Long carId);
}
