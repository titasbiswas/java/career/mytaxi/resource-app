package com.mytaxi.dataaccessobject;

import java.util.List;

import com.mytaxi.datatransferobject.DriverSearchDTO;
import com.mytaxi.domainobject.DriverDO;

public interface DriverRepositoryCustom
{
    List<DriverDO> searchDriver(DriverSearchDTO searchDTO);

}
