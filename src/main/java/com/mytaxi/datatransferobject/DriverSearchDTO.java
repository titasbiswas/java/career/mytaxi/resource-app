package com.mytaxi.datatransferobject;

import lombok.Data;
import com.mytaxi.domainvalue.OnlineStatus;


@Data
public class DriverSearchDTO
{
    private String userName;
    private OnlineStatus  onlineStatus;

    private CarDTO car = new CarDTO();
}
