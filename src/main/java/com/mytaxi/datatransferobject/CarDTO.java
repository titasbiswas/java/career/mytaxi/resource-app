package com.mytaxi.datatransferobject;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainvalue.EngineType;
import com.mytaxi.domainvalue.Manufacturer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CarDTO
{

    @JsonIgnore
    private Long id;

    private String licencePlate;

    private Integer seatCount;

    private BigDecimal rating;

    private Boolean convertible;

    private EngineType engineType;

    @Builder.Default
    private Manufacturer manufacturer = new Manufacturer();


    public CarDTO(CarDO carDO)
    {
        this(carDO.getId(), carDO.getLicencePlate(), carDO.getSeatCount(), carDO.getRating(), carDO.getConvertible(), carDO.getEngineType(), carDO.getManufacturer());
    }


    @JsonProperty
    public Long getId()
    {
        return this.id;
    }

}
