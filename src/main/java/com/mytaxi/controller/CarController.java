package com.mytaxi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.exception.rest.EntityNotFoundException;
import com.mytaxi.service.car.CarService;

@RestController
@RequestMapping("v1/cars")
public class CarController
{
    private final CarService carService;


    @Autowired
    public CarController(final CarService carService)
    {
        this.carService = carService;
    }


    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CarDTO> createCar(@RequestBody CarDTO carDTO)
    {
        return ResponseEntity.status(HttpStatus.CREATED).body(new CarDTO(carService.saveCar(new CarDO(carDTO))));
    }


    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CarDTO> getCarById(@PathVariable Long id) throws EntityNotFoundException
    {
        CarDO carDO = carService.getCarByid(id).map(car -> {
            return car;
        }).orElseThrow(() -> new EntityNotFoundException(CarDO.class, "id", id.toString()));
        return ResponseEntity.status(HttpStatus.OK).body(new CarDTO(carDO));
    }


    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CarDTO> updateCar(@PathVariable Long id, @RequestBody CarDTO carDTO) throws EntityNotFoundException
    {
        CarDO carDO = carService.getCarByid(id).map(car -> {

            if (null != carDTO.getConvertible())
                car.setConvertible(carDTO.getConvertible());
            if (null != carDTO.getEngineType())
                car.setEngineType(carDTO.getEngineType());
            if (null != carDTO.getLicencePlate())
                car.setLicencePlate(carDTO.getLicencePlate());
            if (null != carDTO.getManufacturer().getManufacturedBy())
                car.getManufacturer().setManufacturedBy(carDTO.getManufacturer().getManufacturedBy());
            if (null != carDTO.getRating())
                car.setRating(carDTO.getRating());
            if (null != carDTO.getSeatCount())
                car.setSeatCount(carDTO.getSeatCount());

            return carService.saveCar(car);
        }).orElseThrow(() -> new EntityNotFoundException(CarDO.class, "id", id.toString()));

        return ResponseEntity.status(HttpStatus.OK).body(new CarDTO(carDO));
    }
    
    @DeleteMapping(value = "/{id}")
    ResponseEntity<Void> deleteCarById(@PathVariable Long id) throws EntityNotFoundException
    {
        CarDO car = carService.getCarByid(id).orElseThrow(() -> new EntityNotFoundException(CarDO.class, "id", id.toString()));
        carService.deleteCar(car);
        return ResponseEntity.noContent().build();
    }
}
