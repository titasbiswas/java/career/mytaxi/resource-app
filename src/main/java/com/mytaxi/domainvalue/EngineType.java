package com.mytaxi.domainvalue;

public enum EngineType
{
    PETROL, DIESEL, HYBRID, ELECTRIC
}
