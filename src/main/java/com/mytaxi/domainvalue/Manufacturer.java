package com.mytaxi.domainvalue;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Embeddable
@Data
public class Manufacturer
{
    @Column(name="manufactured_by")
    private String manufacturedBy;
}
