package com.mytaxi.domainobject;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.format.annotation.DateTimeFormat;

import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.domainvalue.EngineType;
import com.mytaxi.domainvalue.Manufacturer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "cars")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CarDO extends Auditable
{
    @Id
    //@GeneratedValue(strategy=GenerationType.IDENTITY)
    @GeneratedValue
    private Long id;

    @Column(name = "license_plate")
    private String licencePlate;

    @Column(name = "seat_count")
    private Integer seatCount;

    @Column
    private BigDecimal rating;

    @Column
    private Boolean convertible;

    @Enumerated(EnumType.STRING)
    @Column(name = "engine_type")
    private EngineType engineType;

    @Embedded
    @Builder.Default
    private Manufacturer manufacturer = new Manufacturer();
    
    @Column(name = "date_created")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime creationDate;


    public CarDO(CarDTO car)
    {
        this(car.getId(), car.getLicencePlate(), car.getSeatCount(), car.getRating(), car.getConvertible(), car.getEngineType(), car.getManufacturer(),  ZonedDateTime.now());
    }

}
