package com.mytaxi.service.car;

import java.util.Optional;

import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.domainobject.CarDO;

public interface CarService
{
    Optional<CarDO> getCarByid(Long id);
    CarDO saveCar(CarDO carDO);
    void deleteCar( CarDO carDO);


}
