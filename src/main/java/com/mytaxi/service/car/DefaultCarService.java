package com.mytaxi.service.car;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.mytaxi.dataaccessobject.CarRepository;
import com.mytaxi.domainobject.CarDO;

@Service
public class DefaultCarService implements CarService
{
    private final CarRepository carRepo;


    public DefaultCarService(final CarRepository carRepo)
    {
        this.carRepo = carRepo;
    }


    @Override
    public Optional<CarDO> getCarByid(Long id)
    {
        return carRepo.findById(id);
    }


    @Override
    public CarDO saveCar(CarDO carDO)
    {
        return carRepo.save(carDO);
    }


    @Override
    public void deleteCar(CarDO carDO)
    {
        carRepo.delete(carDO);

    }

}
