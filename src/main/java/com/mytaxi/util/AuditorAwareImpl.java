package com.mytaxi.util;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;

public class AuditorAwareImpl implements AuditorAware<String>
{
    @Override
    public Optional<String> getCurrentAuditor()
    {
        //TODO - implement custom claim in auth server to retrieve current username
        return Optional.ofNullable("mytaxiuser");
    }
}
