package com.mytaxi.integration;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.hasSize;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AppIntegrationTest
{
    @Autowired
    private MockMvc mvc;
    
    
    //TODO - This access token to be changed in real time
    private final String ACCESS_TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTA0NDQ0NzUsInVzZXJfbmFtZSI6ImFkbWluIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiJdLCJqdGkiOiJlM2RhNzhhNy1iMjYzLTRiZjgtOTBkZC05ZGYzODMyOGRmYjkiLCJjbGllbnRfaWQiOiJteXRheGljbGllbnQiLCJzY29wZSI6WyJ0cnVzdCIsInJlYWQiLCJ3cml0ZSJdfQ.zORI6O-6Z8LjPkYiWZMvO7NC5-zz7F5kAMsG7QAroto";
    
    
    @Before
    public void setupTestPrerequisites() throws Exception {
        mvc.perform(put("/v1/drivers/allocate/4/1?access_token="+ACCESS_TOKEN));
        mvc.perform(put("/v1/drivers/allocate/5/5?access_token="+ACCESS_TOKEN));
        
    }
    
    
    @Test
    public void testGetSearchedDrivers_shouldThrowBadRequest() throws Exception {

        mvc.perform(post("/v1/drivers/search?access_token="+ACCESS_TOKEN).accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("fjfjfj"))
                .andExpect(status().isBadRequest());
    }
    
    @Test
    public void testGetSearchedDrivers_shouldReturnTwoDrivers() throws Exception {

        mvc.perform(post("/v1/drivers/search?access_token="+ACCESS_TOKEN).accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{\"car\":{\"engineType\":\"PETROL\"}}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }
    
    @Test
    public void testGetSearchedDrivers_shouldReturnOneDriver_afterOneDeAllocation() throws Exception {

        mvc.perform(put("/v1/drivers/deallocate/4?access_token="+ACCESS_TOKEN))
        .andExpect(status().isOk());
        
        mvc.perform(post("/v1/drivers/search?access_token="+ACCESS_TOKEN).accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{\"car\":{\"engineType\":\"PETROL\"}}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));
    }
    
    @After
    public void cleanUp() throws Exception {
        mvc.perform(put("/v1/drivers/deallocate/4?access_token="+ACCESS_TOKEN));
        mvc.perform(put("/v1/drivers/deallocate/5?access_token="+ACCESS_TOKEN));
        
    }

}
