package com.mytaxi.dataaccessobject;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.mytaxi.datatransferobject.DriverSearchDTO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.EngineType;
import com.mytaxi.domainvalue.OnlineStatus;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DriverRepositoryTest
{
    @Autowired
    private DriverRepository driverRepository;
    
    @Before
    public void setupTestPrerequisites() {
        driverRepository.allocateCar(4L, 1L);
        driverRepository.allocateCar(5L, 5L);
        
    }
    
    @Test
    public void testAllocateCar() {
        //driverRepository.allocateCar(4L, 1L);
        assertEquals(new Long(1), driverRepository.findById(4L).get().getCar().getId());
    }
    
   @Test
   public void testSearchDrivers() {
      DriverSearchDTO searchDTO;
      List<DriverDO> testData;
      
      searchDTO = new DriverSearchDTO();
      searchDTO.setOnlineStatus(OnlineStatus.ONLINE);
      testData =driverRepository.searchDriver(searchDTO);
      assertEquals(4, testData.size());
      
      searchDTO = new DriverSearchDTO();
      searchDTO.setUserName("driver01");
      testData =driverRepository.searchDriver(searchDTO);
      assertEquals(1, testData.size());
      
      searchDTO = new DriverSearchDTO();
      searchDTO.getCar().setEngineType(EngineType.PETROL);
      testData =driverRepository.searchDriver(searchDTO);
      assertEquals(2, testData.size());
      
      searchDTO = new DriverSearchDTO();
      searchDTO.getCar().setConvertible(true);
      testData =driverRepository.searchDriver(searchDTO);
      assertEquals(1, testData.size());
      
      searchDTO = new DriverSearchDTO();
      searchDTO.getCar().setLicencePlate("AB45ER");
      testData =driverRepository.searchDriver(searchDTO);
      assertEquals(1, testData.size());
      
      searchDTO = new DriverSearchDTO();
      searchDTO.getCar().getManufacturer().setManufacturedBy("NISSAN");
      testData =driverRepository.searchDriver(searchDTO);
      assertEquals(1, testData.size());
      
      searchDTO = new DriverSearchDTO();
      searchDTO.getCar().setRating(new BigDecimal("3.5"));
      testData =driverRepository.searchDriver(searchDTO);
      assertEquals(1, testData.size());
      
      searchDTO = new DriverSearchDTO();
      searchDTO.getCar().setSeatCount(6);
      testData =driverRepository.searchDriver(searchDTO);
      assertEquals(1, testData.size());
   }
   
   @After
   public void cleanUp() {
       DriverDO driverDO =  driverRepository.findById(4L).get();
       driverDO.setCar(null);
       driverRepository.save(driverDO);
       driverDO =  driverRepository.findById(5L).get();
       driverDO.setCar(null);
       driverRepository.save(driverDO);
   }

}
